
module.exports = function($){

	function sendMessage(socket, data) {

		for (var i = 0; i < clients.length; i++) {

			if (clients[i].name == data.to) {

				var to = clients[i].sid;
				io.sockets.socket(to).emit('msg', {to: data.to, from: data.from, message: data.message, datetime: data.datetime});
			};
		};
	};

	function log(string) {

		var dateTime = new Date(),
		time = dateTime.getHours() + ':' + dateTime.getMinutes() + ':' + dateTime.getSeconds();

		console.log(' ' + time + ' - ' + string);
	};
};