
var express, app, server, fs, path, request, ws, nodemailer, mongojs;

var port = 3002,
	ip = 0;

express = require('express');
app = express();
request = require('request');
ws = require('socket.io');
mongojs = require('mongojs');
nodemailer = require("nodemailer");

// File upload
path = require('path'),
fs = require('fs');

// Connecting mongoDB
var db = mongojs('corona:coronadbpass@ds037698.mongolab.com:37698/hungryrun');

// Email settings
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "jsdev.contact@gmail.com",
        pass: "Smoove719"
    }
});

// Configuration
app.configure(function(){

	app.use(express.urlencoded());
	app.use(express.json());

	app.use(express.cookieParser()); // Cookies

	app.use(express.logger({ format: ' :req[ip] :date (:response-time ms): :method :url' })); // Logging
	app.use(express["static"]('./static')); // Static files
});

// Renders main
app.get('/', function(req, res) {

	res.render('index');
});

// Set server
console.log('\n - Welcome to Shooter!');
server = app.listen(port, ip);
console.log(' - HTTP server started on port: ' + port + ', ip: ' + ip + '\n');
io = ws.listen(server);

// Only websockets
io.set('transports', ['websocket']);
console.log(' - Using only websockets\n');

// Routing
fs.readdirSync('./routes').forEach(function(file) {

	var name = file.substr(0, file.indexOf('.'));
	
	require('./routes/' + name)({
		app: app,
		request: request,
		db: db,
		io: io,
		smtpTransport: smtpTransport
	});

	console.log(' - Controller loaded: ' + name + '.js');
});

console.log('\n - Server fully loaded...\n');
console.log('\n');